<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cabinet de Curiosités</title>
    <link rel="stylesheet" href="stylesheet.css">
    <script src="main.js"></script>
</head>
<body>

    <div id='head'>
        <p>Cabinet de Curiosités</p>
    </div>

    <div id='container'>
        <a href="page-perso/object-1.html"><div class="object active a1" id="o1"><img src="" alt=""></div></a>
        <a href="page-perso/object-2.html"><div class="object active a2" id="o2"><img src="" alt=""></div></a>
        <a href="page-perso/object-3.html"><div class="object active a3" id="o3"><img src="" alt=""></div></a>
        <a href="page-perso/object-4.html"><div class="object" id="o4"><img src="" alt=""></div></a>
        <a href="page-perso/object-5.html"><div class="object" id="o5"><img src="" alt=""></div></a>
        <a href="page-perso/object-6.html"><div class="object" id="o6"><img src="" alt=""></div></a>
        <a href="page-perso/object-7.html"><div class="object" id="o7"><img src="" alt=""></div></a>
    </div>

    <div class="label" id='l1'><p>Label 1</p></div>
    <div class="label labelactive" id='l2'><p>Label 2</p></div>
    <div class="label" id='l3'><p>Label 3</p></div>
    <div class="label" id='l4'><p>Label 4</p></div>
    <div class="label" id='l5'><p>Label 5</p></div>
    <div class="label" id='l6'><p>Label 6</p></div>
    <div class="label" id='l7'><p>Label 7</p></div>

    <div class="cont-btn">
        <div class="btn-nav left">←</div>
        <div class="btn-nav right">→</div>
    </div>

    <div id='about'>
        <p>À propos</p>
    </div>
    
</body>
</html>