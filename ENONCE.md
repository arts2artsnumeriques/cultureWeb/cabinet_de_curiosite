##  Enoncé:



### [Cabinet de Curiosités]

témoignages curieux d’un monde curieux pour ceux qui sont curieux



#### \1. Énoncé



Le [cabinet de curiosités](https://fr.wikipedia.org/wiki/Cabinet_de_curiosités) était un meuble contenant des objets catégorisés qui a leur tour contenaient des histoires révélant un aspect de l’histoire naturelle, sociale et/ou culturelle de l’homme.

Il est proposé d’en faire aujourd’hui une transposition ou une interprétation contemporaine, étroitement liée à la technologie et ce, même si l’objet n’est pas de fabrication humaine.

Le but de l’exercice est de développer des compétences en recherche et analyse et en production de documents illustratifs, graphiques et analytiques. Pour ce faire une méthode de travail est proposée et le travail en groupes est privilégié.

Les objets sont tous porteurs d’une histoire qui en soi peut potentiellement représenter le départ d’une démarche artistique et de création , le but étant de ne pas poursuivre jusqu’à cette réinterprétation qui confinerait l’objet à un rôle de référence. L’aspect créatif se retrouve dans la mise en œuvre et la formalisation d’un site web comprenant vidéos, texte et hyperliens, d’une charte graphique et d’une structure de navigation, d’une scénographie d’exposition et des moyens techniques à mettre en œuvre à la fois pour la production des documents et leur monstration.



#### \2. Étapes de travail



##### 2.1. Présentation des objets (session 1-2-3)

Les objets sont soumis aux étudiants par les professeurs. Ceux-ci sont invités à proposer leurs objets (session 2). La rareté de l’objet (curiosité) ou son aspect méconnu sont privilégiés, l’objet doit révéler une histoire relative au monde, à la culture ou à la société humaine au-delà d’une histoire personnelle ou familiale.



##### 2.2. Analyse/recherche sur les objets (sessions 3-4-5-6)

Très peu d’indices sont fournis aux étudiants, c’est à eux de découvrir l’origine des objets et leurs multiples significations, historiques, culturelles, transposées à l’époque contemporaine, dans les implications et les questionnements qu’elles peuvent soulever. Tous les étudiants doivent mener une recherche. Le partage des découvertes se fait en session d’atelier, comme pratique de la présentation orale. A partir de la session 4, on dépasse l’oral uniquement et des documents peuvent être produits.



##### 2.3. Formation des équipes (session 3)

En fonction de l’intérêt (curiosité) de chaque étudiant, des groupes se forment autour des objets. Ce sont les équipes de travail, il n’est pas exclu qu’un étudiant soit solitaire, même si l’équipe facilite la répartition des tâches.



##### 2.4. Électronique/fonctionnement (session 3) - Valkiri

Certains objets peuvent nécessiter un dispositif qui permet de faire une démonstration de leur fonctionnement. L’électronique peut intervenir, le démontage et le ré-assemblage. Des objets contextuel/informatif peuvent être conçus pour intégrer les plans vidéos. D’autres être spécifiques à la scénographie de l’exposition.



##### 2.5. Synopsis / Storyboard (sessions 5-6-7)

Élaboration du [synopsis](https://fr.wikipedia.org/wiki/Synopsis) de la vidéo. Ce synopsis peut être le texte pour le site web. Celui-ci devra néanmoins ne pas révéler toute l’histoire de l’objet mais juste constituer une accroche. Dans un second temps, le [storyboard](https://fr.wikipedia.org/wiki/Storyboard) est dressé, plans et minutages, voix off et/ou sons, incrustrations graphiques.



##### 2.6. Charte graphique / Structure site (session 4) – Etienne Ozeray

Éléments de la charte : Ils seront identifiés en session, la structure est établie en parallèle. Discussion autour des plateformes Facebook, Instagram et autres, pour évaluer l’utilité d’un détournement. [Clickbait](https://fr.wikipedia.org/wiki/Piège_à_clics), « 10 objets que vous n’avez jamais vu ». Rappeler du bon usage des hyperliens.



##### 2.7. Prises de vues (session 8-9-10-11) et de son

Un studio de prise de vue est monté. Il comprend 2 [caméras](https://fr.canon.be/cameras/eos-m6-mark-ii/) , un choix d’objectifs dont un macro éclairé, 3 [spots à Led](https://litra.com/pages/litrapro), 2 trépieds vidéo et un [slider 260mm motorisé](https://www.gudsen.com/moza-slypod), un [gimball 3 axes motorisé](https://www.gudsen.com/moza-aircross-2), 3 pieds d’éclairage, deux toiles de fond (vert,bleu,blanc,noir), un clap (Ipad ou réel) et divers accessoires à considérer et/ou intégrer dans la prise de vue (équerres/règles, millimétré, afficheur) autant d’éléments qui peuvent influencer de retour la charte graphique.

Un micro USB avec pied, filtre antipop et écran anti réflexion pour la voix off est également prévu. Un enregistreur mobile également.



##### 2.8. Montage

Introduction [openshot](https://www.openshot.org/fr/download/), (ou Première ou Finalcut ou??) montage de base, composition avec graphiques, timing, bande son. (voir avec JB Libert et Martin, alphabétisation numérique, possible?).



##### 2.9. Itérations de prototypes

Le site web se fait par itérations, la vidéo aussi. Enfin la scénographie est expérimentée élément par élément. Le principe de la réévaluation permanente permet de parfaire le résultat et de consolider les idées dans un tout cohérent. (Martin pour la lecture vidéo sur raspberry et le déclenchement via wifi?).



#### \3. Résultat



Le résultat est un site web avec un structure/navigation qui reflète le cabinet de curiosités. Parallèlement et complémentairement, une installation est conçue, elle peut atteindre le vrai statut de scénographie, même si ce n’est pas l’objectif principal. Il sera fait usage d’écrans et des objets, éventuellement en superposition. La structure du site peut être transposée spatialement. Le site peut également être transposé en brochure/catalogue d’exposition. Une stratégie de communication et de publication peut être mise en place.
