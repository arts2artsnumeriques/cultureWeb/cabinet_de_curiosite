<?php
  include('librairies/Parsedown.php');

  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);



  $dirProjet = array();
  $MyDirectory = opendir('data') or die('Erreur');
  while($Entry = readdir($MyDirectory)) {
    if(is_dir($MyDirectory.'/'.$Entry) && $Entry != '.' && $Entry != '..') {
    } elseif ($Entry !='.' && $Entry != '..') {
      array_push($dirProjet,$Entry);
    }
  }
  closedir($MyDirectory);

  sort($dirProjet);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/main.css">
    <title>Document</title>
    <link rel="shortcut icon" href="img/an_logo.png" type="image/png">
  </head>
  
  <body class="home">


    <div id="main_container">
      <?php foreach ($dirProjet as $projet) { ?>
        <a href="object.php?dir=<?= $projet ?>"><img class="object" src="data/<?= $projet ?>/home.jpg" alt=""></a>
      <?php } ?>
    </div>

    <div class="logo">
      <img src="img/an_logo.png" alt="">
    </div>

    <div class="gitlab">
      <a href="https://gitlab.com/arts2artsnumeriques/cultureWeb/cabinet_de_curiosite">
        <img src="img/GitLab_Logo.png">
      </a>
    </div>


    <div class="curiosity_object" id='titre'><p id='title'>Cabinet de Curiosité</p></div>
    <script src="js/main.js"></script>
    </body>
</html>
