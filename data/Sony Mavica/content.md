### Alex Simescu et Meline Viola

Les premiers appareils de la ligne Sony Mavica sont sortis en 1981, ils étaient analogiques et utilisaient une technologie de capture d’images par vidéo. Le terme Mavica provient de *Magnetic Video Camera*. Les Digital Mavica sont les héritiers de ce concept, employant un système de sauvegarde des photographies digitales sur des supports amovibles comme la disquette 3,5 pouces.

Pour la sortie de Mavica Digital FD7, en 1997 les créateurs ont mis en place un système de disquette qui est un support de stockage de données informatiques amovible, qui peut aussi être appelée disque souple (*floppy disk* en anglais) en raison de la souplesse des premières générations (8 et 5,25 pouces) et par opposition au disque dur. Elle est tellement iconique qu’à la fin du 20e siècle de nombreux logiciels ont choisi d’utiliser l’icône d’une disquette pour indiquer l’opération de sauvegarde. Ce support employé par les appareils Sony Mavica permet le transfert direct des photos vers l’ordinateur.

Il faut cependant savoir que toute cette nouvelle technologie a été intégrée dans le style d’un appareil photo classique afin de ne pas déboussoler les acheteurs, mais ce choix de design va mettre en péril son succès. À sa sortie l’appareil va vite passer à la trappe et ne connaîtra pas le succès qu’il aurait pu avoir. 

C’est donc suite à son histoire que nous allons faire un bond dans le passé plus précisément dans les années '80 pour redécouvrir l’incroyable appareil photo à disquettes Mavica Digital FD7! C’est dans un univers très décalé, voire disco que nous allons vous dévoiler cette merveille de la technologie.

