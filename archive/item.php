<?php
  include('librairies/Parsedown.php');

  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);

  $dir = $_GET['dir'];

?>



<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Cabinet de curiosité</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style/main.css">
    <link rel="stylesheet" href="style/<?= $dir ?>.css">
  </head>
  <body>

  
    <?php
      $Parsedown = new Parsedown();
      $path = 'data/'.$dir . '/';
      $MyDirectory = opendir($path) or die('Erreur');
      while($Entry = readdir($MyDirectory)) {
        (preg_match('#\.(md)$#i', $Entry)) ? $text = $Entry : '';
        // if( $Entry != '.' && $Entry != '..' && preg_match('#\.(jpe?g|gif|png|svg)$#i', $Entry)) {
        //   array_push($images,$Entry);
        // }
      }
      $content = file_get_contents($path . $text);
      $content = $Parsedown->text($content);
      $content = str_replace('src="', 'src="' . $path, $content);

      echo $content;
    ?>
    <script src="" async defer></script>
  </body>
</html>