<?php
	
$config_db=dirname(__FILE__)."/db.txt";  // location of the database file, change this to something less obvious
$config_site="http://www.crivionweb.com"; // absolute url for your domain here
$config_theme=dirname(__FILE__)."/theme.html";  // location of the "theme" file, a single html page
$config_date="M jS, Y"; // how to format the date function, localize it as needed
 
$found=false; // defaults to page not found
 
$url=basename ($_SERVER['REQUEST_URI']);// this returns the actual file requested by the user
 
if($url=='') $url='/';                  // force home page to be the single slash
 
$fp=@fopen($config_db,"rb"); // the @ prevents errors from showing
 
 
if(!$fp===false && $url!="404.html") // this check saves time if this is a 404 redirect
{
 $data=fread($fp,filesize($config_db));
 if(! $data===false )
 {// file was read in, we can parse it
 
     $articles=explode("#99#",$data); // put each article in an array
 
     foreach($articles as $article)
     {
     /* now loop all articles looking for the one that contains the url
     play it safe including #00# markers before and after the url, so that we can link between pages without problems */
     $pos=strpos($article, "#00#".$url."#00#");
      if($pos !== false)
      {// this is the article we were looking for
 
 
          $found=true;
          // now explode the article and exit the loop to save time
          $items=explode("#00#",$article);
          break;
      }
     }
     fclose($fp);
 }
}
 
else // issue a 404 redirect and die
{
header ("HTTP/1.0 404 Not Found 404.html");
exit;
}