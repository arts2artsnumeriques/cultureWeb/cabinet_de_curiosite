<?php
    include("librairies/Parsedown.php");
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    $dir = $_GET['dir'];

    $Parsedown = new Parsedown();
    $chemin = "data/" ."$dir". "/";
    $allImgPath = $chemin. 'img/'; // chemin pour trouver les img d'un projet

    $content = file_get_contents("$chemin". 'content.md');
    $content = $Parsedown->text($content);

    $dirProjet = array(); // tableau avec les noms des projets
    $imgProjet = array(); // tableau avec toute les img d'un projet

    $MyDirectory = opendir('data') or die('Erreur');
    while($Entry = readdir($MyDirectory)) {
      if(is_dir($MyDirectory.'/'.$Entry) && $Entry != '.' && $Entry != '..') {
      } elseif ($Entry !='.' && $Entry != '..') {
        array_push($dirProjet,$Entry);
      }
    }
    closedir($MyDirectory);

    sort($dirProjet);

    $myImgDir = opendir($allImgPath) or die('Erreur');
    while($imgEntry = readdir($myImgDir)) {
        if(is_dir($myImgDir.'/'.$imgEntry) && $imgEntry != '.' && $imgEntry != '..') {
        } elseif ($imgEntry !='.' && $imgEntry != '..') {
          array_push($imgProjet,$imgEntry);
        }
    }
    closedir($myImgDir);

    sort($imgProjet);


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="style/object.css">
    <title>Page des objets</title>
    <link rel="shortcut icon" href="img/an_logo_bck_dark.png" type="image/png">
</head>
<body>

    <a href="index.php"><div class="logo">
        <img src="img/an_logo.png" alt="">
    </div></a>
    <div id="titleObject">
        <h1><?= "$dir" ?></h1>
    </div>
    <div class="synopsis">
        <?= $content ?>
    </div>
    <div class="object">
        <?php 
        $video = $chemin.'/video/video.mp4';
        if (file_exists($video)) { 
        ?>
            <video controls autoplay="true" muted loop>
                <source src="<?= $video ?>" type="video/mp4">
            </video>
         <?php } ?>

        <?php foreach ($imgProjet as $imgs) { ?>
            <img src="<?= $allImgPath. $imgs?>" alt="">
        <?php } ?>
    </div>
    <script src="js/main.js"></script>
</body>
</html>
