#  cabinet_de_curiosite

Témoignages curieux d’un monde curieux pour ceux qui sont curieux.



Version Live:
https://tgm.happyngreen.fr/project/AN/cabinet_de_curiosite/



[Enoncé](https://gitlab.com/arts2artsnumeriques/cultureWeb/cabinet_de_curiosite/-/blob/master/ENONCE.md)



## Voici les liens actifs pour le projet Cabinet de curiosité.

- le framapad contenant les discutions autour du projet:*

   https://semestriel.framapad.org/p/cabinetdecuriosite-9j4x?lang=fr

- *le projet gitlab contenant le code:*

  https://gitlab.com/arts-_arts_num_cabinet_de_curiosit

- Dépendence:

  *Librairie Masonry* : https://masonry.desandro.com/

  *Parsdown*: https://github.com/erusev/parsedown
